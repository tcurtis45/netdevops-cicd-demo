# Lab 02 - Basic Network Automation

In this lab we will learn how to leverage Ansible using gitlab-runners to execute our scripts in the pipeline.  The 
shell environment that gitlab-runner is running in already has Ansible, paramiko, and a few other packages installed that 
we will leverage directly from the command line.

**Objective**: Understand how to launch Ansible from gitlab-runners as a SHELL executor against a network environment. You will 
build the inventory file, playbook, variables, and the `.gitlab-ci.yml` files for an entire pipeline.

## Table of Contents

[[_TOC_]]

## Device Assignments
Refer to your POD number assignment to reference 
Each user will be assigned a POD number.  Reference that number in the chart below for Cisco IOS Assignment.  Login via SSH 
to your lab device(s) to make sure they are accessible and review the starting configuration (or lack thereof).  The devices 
should only be configured with a management interface on Ethernet0/0 and basic configuration to allow SSH.


```
ssh -oKexAlgorithms=+diffie-hellman-group14-sha1 cisco@10.216.107.1XX
```

| POD  | Cisco IOS Device  | Runner VM |      |  POD  |  Cisco IOS Device  | Runner VM | 
|---|---|---|---|---|---|---|
|  **POD01**  | 10.216.107.101  | 10.207.63.201 |  -  |  **POD21**  | 10.216.107.121  | 10.207.63.221 |
|  **POD02**  | 10.216.107.102  | 10.207.63.202 |  -  |  **POD22**  | 10.216.107.122  | 10.207.63.222 |
|  **POD03**  | 10.216.107.103  | 10.207.63.203 |  -  |  **POD23**  | 10.216.107.123  | 10.207.63.223 |
|  **POD04**  | 10.216.107.104  | 10.207.63.204 |  -  |  **POD24**  | 10.216.107.124  | 10.207.63.224 |
|  **POD05**  | 10.216.107.105  | 10.207.63.205 |  -  |  **POD25**  | 10.216.107.125  | 10.207.63.225 |
|  **POD06**  | 10.216.107.106  | 10.207.63.206 |  -  |  **POD26**  | 10.216.107.126  | 10.207.63.226 |
|  **POD07**  | 10.216.107.107  | 10.207.63.207 |  -  |  **POD27**  | 10.216.107.127  | 10.207.63.227 |
|  **POD08**  | 10.216.107.108  | 10.207.63.208 |  -  |  **POD28**  | 10.216.107.128  | 10.207.63.228 |
|  **POD09**  | 10.216.107.109  | 10.207.63.209 |  -  |  **POD29**  | 10.216.107.129  | 10.207.63.229 |
|  **POD10**  | 10.216.107.110  | 10.207.63.210 |  -  |  **POD30**  | 10.216.107.130  | 10.207.63.230 |
|  **POD11**  | 10.216.107.111  | 10.207.63.211 |  -  |  **POD31**  | 10.216.107.131  | 10.207.63.231 |
|  **POD12**  | 10.216.107.112  | 10.207.63.212 |  -  |  **POD32**  | 10.216.107.132  | 10.207.63.232 |
|  **POD13**  | 10.216.107.113  | 10.207.63.213 |  -  |  **POD33**  | 10.216.107.133  | 10.207.63.233 |
|  **POD14**  | 10.216.107.114  | 10.207.63.214 |  -  |  **POD34**  | 10.216.107.134  | 10.207.63.234 |
|  **POD15**  | 10.216.107.115  | 10.207.63.215 |  -  |  **POD35**  | 10.216.107.135  | 10.207.63.235 |
|  **POD16**  | 10.216.107.116  | 10.207.63.216 |  -  |  **POD36**  | 10.216.107.136  | 10.207.63.236 |
|  **POD17**  | 10.216.107.117  | 10.207.63.217 |  -  |  **POD37**  | 10.216.107.137  | 10.207.63.237 |
|  **POD18**  | 10.216.107.118  | 10.207.63.218 |  -  |  **POD38**  | 10.216.107.138  | 10.207.63.238 |
|  **POD19**  | 10.216.107.119  | 10.207.63.219 |  -  |  **POD39**  | 10.216.107.139  | 10.207.63.239 |
|  **POD20**  | 10.216.107.120  | 10.207.63.220 |  -  |  **POD40**  | 10.216.107.140  | 10.207.63.240 |

[Back to Top](#table-of-contents)

## Step 1: Import new repository
In Gitlabs, go back to your home page which shows all of your project by clicking on the Gitlab logo.  

* Import Project from Gitlab.com

Once back on the Project page, click **New Project** in the upper right corner.  Choose **Import project** to pull in from 
another repository.   Choose **Repository by URL** and then paste the following URL into the 'Git repository URL' field. 
The Project name, and project slug should auto populate.  Go ahead and change the visilibity to 'Public' so we can all 
see your repository.

```
https://gitlab.com/tcurtis45/netdevops-cicd-demo.git
```

* Create project

Click the **Create project** button at the bottom.

[Back to Top](#table-of-contents)

## Step 2: Edit Inventory

The repository you have just pulled down is a starting point for Lab02 and further labs.  The root directory contains the 
`inventory.yml` file and `ansible.cfg` files.  The `ansible.cfg` file has configuration settings for Ansible, mainly telling 
it to ignore host key checking. Individual labs are located in each respective folder.  Go to the repository files and 
drill down to the **Lab02-Basic_Network_Automation** folder.  In this folder you will see the following files.  `snmp.yml` 
is an Ansible playbook to make SNMP changes.  `linter.sh `you saw in the previous lab uses ```yamllint``` to validate YAML 
files.  There is also a `group_var/all.yml` file which has the variables applicable to all devices 
in our inventory. 

![Lab02 - Starting Files](../images/Lab02-Starting_files.jpg)

* Edit the inventory file to include your POD routers

Go to the root directory of your repository -> `inventory.yml`.  Choose the **Open in Web IDE** button.  Change 
the hostnames and IP addresses for pod01 to your respective POD numbers and change the ansible_host IP addresses to match 
your assignment.  

* Commit the changes

Click the **Commit changes** button.  Change the commit branch to ```main``` and then click Commit.   

* Review the playbook snmp.yml

Look at the playbook for snmp.yml.  Notice it is a standard Ansible playbook.  The actual SNMP strings are referenced using 
variables.  The variables that are referenced here can be stored in either group variables or host variables.  
For this lab they are stored in group variables.  Review the group_vars/all.yml file to review the SNMP strings that will be used. 

NOTE:  You should NEVER put SNMP strings directly in your GIT repository, or any sensetive information for that matter.  Use environmental variables
for any sensitive things (usernames, passwords, snmp strings, api keys, etc).

## Step 3: Edit the .gitlab-ci.yml file to add Ansible job to pipeline

Go back to the repository and Lab02-Basic_Network_Automation.  

* Edit Lab02-Basic_Network_Automation/.gitlab-ci.yml file

Click the `.gitlab-ci.yml` file and click **Open in Web IDE**.

For this lab we are going to create our first stage as Validation which will include the linter, and then deploy which 
will push the SNMP strings to the devices configured in our inventory file. Copy and paste the snippet below into 
`.gitlab-ci.yml`.  **Commit changes** and then **Commit** to the main branch.  This will kick off your CI/CD process again.

.gitlab-ci.yml
```yaml
---
stages:
    - validate
    - deploy

validate YAML:
    stage: validate
    script:
        - cd Lab02-Basic_Network_Automation
        - bash linter.sh

Deploy SNMP:
    stage: deploy
    script:
        - ansible-playbook Lab02-Basic_Network_Automation/snmp.yml

```
NOTE: For both of these scripts we changed directory into the current working directory.  This is because when the CI process 
executes, it is doing so from the root of the repository.  Since each job within the CI process kicks off another bash process, 
each of the jobs the specify command line arguments requiring file in another directory either have to change to the other 
directory or use relative paths for all files.  

* Review pipeline

Click on CI/CD -> Pipelines and look at the most recent pipeline.  

![Lab02 - Update CICD for the first time](../images/Lab02-Update_CICD_First_time.jpg)

Review the new jobs for the validate and deploy stage.  Look at the output for the Deploy SNMP stage to see the results from Ansible.  

![Lab02 - Ansible Results](../images/Lab02-Update_CICD_Ansible_Results.jpg)

[Back to Top](#table-of-contents)

## Step 4: Change the strings and observe the playbook run again

* Change SNMP strings in group_vars/all.yml

From the Repository -> group_vars, open file `all.yml`.  Click **Open in Web IDE** to edit.

Change the SNMP strings to the strings below.

```yaml
---
snmp_password_global_ro: RO_SNMP12345
snmp_password_global_rw: RW_SNMP12345
```

* Commit the file to main branch

Click **Create commit...**.  Update the comments, then click the radio button for 'Commit to main branch' 
before clicking **Commit**.

* Review the CI/CD Pipeline for completion

Go to the CI/CD -> Pipelines screen and view the pipeline that was just created with the name of your comment.  Open the 
Status screen. Expand the Lab2_PIPELINE Downstream stage to see the individual stages and jobs of the Lab 02 pipeline.

![Lab02 - SNMP Change Pipeline](../images/Lab02-SNMP_Change_Pipeline.jpg)

The job completed successfully.  Let's go check what the configuration looks like on the actual router

* SSH to your POD router and check config

Using your SSH client (SecureCRT, Putty, etc), SSH into your POD routers using the assigned IPs from the 10.216.107.x range.

![Lab02 - SNMP Update](../images/Lab02-SNMP_Output-1.jpg)

Notice that we have two sets of SNMP strings.  Although that is technically correct, that is not what we wanted as 
part of our automation pipeline.  The goal of setting the SNMP strings was to rotate SNMP strings on a regular basis.  The 
security requirement is to remove existing strings and update to a new string.

Our playbook right now uses the standard cisco.ios.config module to execute the change. Although this gets the job done, 
 it is not the best way to do SNMP change management.  Let's go to the next Step to use specific SNMP server module 
 from the cisco.ios collection on Ansible. 

[Back to Top](#table-of-contents)

## Step 5: Ansible with specific module for SNMP from cisco.ios

For the next step, instead of using the generic cisco.ios.ios_config module as we did in the previous example, we will use 
the specific SNMP Server module so that Ansible will become aware of WHAT it is doing as opposed to the generic config 
plugin.  

* View the Ansible documentation for cisco.ios plugin

First, go to the Ansible documentation to see the list of modules using the following link.

[cisco.ios.ios Ansible Plugins](https://docs.ansible.com/ansible/latest/collections/cisco/ios/index.html)

Read through the list of plugins.  These are all specific to Cisco IOS and IOS-XE devices.  If these existing modules fit 
your use case, it is usually better to use them than the generic cisco.ios.ios_config module.

* View ios_snmp_server module

Scroll down to the bottom and click on the ios_snmp_server module.  Notice there is A LOT of elements in this module.  
Scroll to the top of the module and look for the table of contents links.  Click on Examples to see lists of examples 
of how to use the module.  Notice this supports both version 2 and version 3 of SNMP.

* Change snmp.yml to use cisco.ios.ios_snmp_server

Go to repository -> Lab02-Basic_Network_Automation -> snmp.yml file and **Open in Web IDE**.  Delete everything and replace 
with the text copied from below. (The only thing that changed is the fourth play (Overwrite SNMP string) but it's easier 
to just replace the whole thing than trying to get formatting and spacing correct.)

```yaml
---
- name: GATHER INFORMATION FROM ROUTERS
  hosts: all
  connection: network_cli
  gather_facts: true

  tasks:
    - name: GATHER ROUTER FACTS
      ios_facts:

    - name: DISPLAY VERSION
      debug:
        msg: "The IOS version is: {{ ansible_net_version }}"

    - name: DISPLAY SERIAL NUMBER
      debug:
        msg: "The serial number is:{{ ansible_net_serialnum }}"

    - name: Overwrite SNMP Strings with new SNMP strings using SNMP module
      cisco.ios.ios_snmp_server:
        config:
          communities:
            - acl_v4: SNMP_RO
              name: "{{ snmp_password_global_ro }}"
              ro: true
            - acl_v4: SNMP_RO
              name: "{{ snmp_password_global_rw }}"
              rw: true
        state: replaced
```
* Commit changes and watch CI pipeline execute

Click **Create commit...**.  Update the comments, then click the radio button for 'Commit to main branch' 
before clicking **Commit**.  Go to CICD -> Pipelines and look at the job that was executed.  Wait for it's completion.

* What went wrong?

The playbook errored out.  What happened?  The error is not obvious why it is not working but indicates a problem with 
the module that we called, cisco.ios.ios_snmp_server.  

* Verify documentation

Let's go back to the Ansible plugin directory [cisco.ios.ios Ansible Plugins](https://docs.ansible.com/ansible/latest/collections/cisco/ios/index.html)
 and look for the module that we just called.  Notice the version number of the Cisco.ios module that supports this command: 2.6.0.
 
![Lab02 - SNMP Module Failed](../images/Lab02-SNMP_Module_Failed.jpg)

How do we verify what version of the module that the server is running?  If you had direct access to the server you could 
run the following line from the command prompt to validate.  

```ansible-galaxy collection list```

You don't have access to the server directly but you can run a command in your playbook to validate the Ansible Galaxy libraries.

* Edit CI Pipeline file to add the ansible-galaxy collection list command

Go to Repositories -> Lab02-Basic_Network_Automation -> .gitlab-ci.yml file and open in the Web IDE.  Add a new job before 
the validate YAML stage called 'Validate Ansible-Galaxy'.  

Copy and replace the text below in the Lab02-Basic_Network_Automation\.gitlab-ci.yml file.
```yaml
---
stages:
    - validate
    - deploy

validate Ansible Galaxy:
    stage: validate
    script:
        - ansible-galaxy collection list

validate YAML:
    stage: validate
    script:
        - cd Lab02-Basic_Network_Automation
        - bash linter.sh

Deploy SNMP:
    stage: deploy
    script:
        - ansible-playbook -i inventory.yml Lab02-Basic_Network_Automation/snmp.yml

```

* Commit this file to the main branch and view pipeline output of new job

Click **Create commit...**.  Update the comments, then click the radio button for 'Commit to main branch' 
before clicking **Commit**.  Go to CICD -> Pipelines and look at the job that was executed.  Drill down to the 'Validate Ansible-Galaxy' 
job and wait for it to be picked up and completed by a runner.  

Look at the output of the ansible-galaxy command and you will notice that the cisco.ios package is at v1.0.3.  

![Lab02 - Ansible-Galaxy](../images/Lab02-Ansible-Galaxy.jpg)

This is the problem with using shell commands to execute your ansible playbooks.  The shell command is dependent on the BASH 
environment of the Linux box it was installed on.  It would not be ideal to have to update the ansible-galaxy library 
(or any other module requirement) on the root system itself everytime you wanted a change.  The much more logical approach 
is to use containers that are specifically configured with necessary libraries to execute the changes.  In the next 
lab we will explore how to use Docker containers in our CI/CD pipeline to ensure we are always using a known good container with all 
necessary dependencies. 

[Back to Top](#table-of-contents)

[Lab03 - Ansible with Docker](Lab03-Ansible_with_Docker/README.md)
