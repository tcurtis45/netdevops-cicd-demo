# NetDevOps - CICD Demo



## Lab Instructions

[Lab01 - Introduction to Gitlab](Lab01-Introduction_to_Gitlab/README.md) - In this lab you will create a basic repository for housing code, create a basic pipeline file, 
execute the pipeline, review results, and update the pipeline.  This section is intended to give you an overview of the gitlab tool 
and basic CI process.

[Lab02 - Basic Network Automation](Lab02-Basic_Network_Automation/README.md) - This lab will have you cloning the core repository (netdevops-cicd-demo), edit the inventory file, 
create ansible pipeline for SNMP configuration, update settings, and view the execution of the pipeline.  You will push the limits 
of the SHELL executor for the gitlab-runner.

[Lab03 - Ansible with Docker](Lab03-Ansible_with_Docker/README.md) - This lab will show you how to use an existing dockerhub image 
that has the necessary components to run Ansible the way we want to.  You will create your own custom gitlab-runner using the DOCKER executor, 
execute the pipeline, and review results.  No knowledge of docker will be required but you will learn a few things about it along the way.

[Lab04 - Proper Issues and Merge Processes](Lab04-Proper_Issues_and_Merge_Processes/README.md) - In this lab you will follow
 an issue request in your repository to branch the MAIN branch into a working branch, make your changes, verify your changes, and then merge 
 them into the MAIN branch again.  In the process you will create notes int he change log and review approval processes.
 
[Lab05 - Putting it all together](Lab05-Putting_it_all_together/README.md) - In the final lab we will take all the lessons previously
 learned and expand by adding a new feature (BGP) to your pod config.  In addition to pushing the configuration, you will validate using 
 a test stage in the CI/CD process.  The final state of this lab will allow you to ping the 'INTERNET' addresses from your Loopback interface on your POD device.